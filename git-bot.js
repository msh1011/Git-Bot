const Discord = require('discord.js');
const client = new Discord.Client();

var fs = require('fs');
var request = require("request")

client.login(JSON.parse(fs.readFileSync('token.json', 'utf8')).token);

class Server {
	/*
	 * Server constructor
	 */
	constructor(id){
		this._id = id;
		this._pullReqs = [];
		this._branches = [];
		this._last_commit = {};
		this._parent_id = "0";
		this._url = "https://gitlab.com/"
	}
	/*
	 *	Helper for loading data to servers at start
	 */
	loadData(data){
		this._id = data._id;
		this._pullReqs = data._pullReqs;
		this._branches = data._branches;
		this._last_commit = data._last_commit;
		this._git_key = data._git_key;
		this._url = data._url;
		this._repo_id = data._repo_id;
		this._parent_id = data._parent_id;
	}

	/*
	 *	Check for new pull requests and 
	 *	send a message to general when 
	 *	there is a new one
	 */
	getPullReq(){
		var options = {
			url: this._url + "api/v4/projects/" + this._repo_id + "/merge_requests?state=opened",
			headers: {
					'Private-Token': this._git_key
			}
		};
		request.get(options, (error, response, body) => {
			var json = JSON.parse(body);
			var length = json.length;
			for (var i = 0; i < length; i++) {
				if(!this._pullReqs.includes(json[i]['sha'])){
					var js = "```New Pull Request Opened by " + json[i]['author']['name'] + " (" + json[i]['project_id'] + "-" + json[i]['id'] + ")" + 
						"\n\n" + json[i]['title'] +
						"\n\n" + json[i]['source_branch'] + " --> " + json[i]['target_branch'] +
						"\n" + "```"
					client.guilds.find('id', this._id).channels.find('name', 'general').send(js, {
							embed: {
									title: "Open on Git-Lab",	
									url: json[i]['web_url'],
							}})
					this._pullReqs.push(json[i]['sha'])
				}
			}
			if(length == 0){
				this._pullReqs = [];
			}
		});
	}
	/*
	 *	Check for any new branches and
	 *	create a text channel for them
	 */
	updateBranches(){
		var options = {
			url: this._url + "api/v4/projects/" + this._repo_id + "/repository/branches",
			headers: {
					'Private-Token': this._git_key
			}
		};
		if(client.guilds.find('id', this._id).channels === undefined){
			return -1;
		}
		if(!client.guilds.find('id', this._id).channels.exists('id', this._parent_id)){
			this._parent_id = "0";
		}
		var t = this;
		if(this._parent_id == "0"){
			var guild = client.guilds.find('id', this._id);
			var prom = Promise.resolve(guild.createChannel('Branches', 'category'));
			this._parent_id = "-1";
			prom.then(function(value) {
				t._parent_id = value.id;
			});
		}
		if(this._parent_id == "0" || this._parent_id == "-1"){
			return -1;
		}
		request.get(options, (error, response, body) => {
			var json = JSON.parse(body);
			var guild = client.guilds.find('id', this._id)
			var channels = guild.channels.findAll('parentID', t._parent_id);
			var new_branches = [];
			for( var i = 0; i < json.length; i++){
				new_branches.push(json[i].name);
				if( this._branches.indexOf(json[i].name) == -1){
					var prom = Promise.resolve(guild.createChannel(json[i].name, 'text'));
					prom.then(function(value) {
						value.setParent(t._parent_id);
						value.parentID = t._parent_id;
					});
					this._last_commit[json[i].name] = json[i].commit.id;
				} else {
					channels.splice( channels.findIndex( (value) => {return value.name === json[i].name}), 1);
				}
			}
			for(var i = 0; i < channels.length; i++){
				guild.channels.find('name', channels[i].name).delete();
			}
			this._branches = new_branches;
		});
	}
	/*
	 *	Check each branch for new commits, 
	 *	then generate a message and send 
	 *	it to the channel for that branch
	 */
	getCommits(){
		if(this._parent_id == "0" || this._parent_id == "-1"){
			return;
		}
		for(var t = 0; t < this._branches.length; t++){
			var options = {
				url: this._url + "api/v4/projects/" + this._repo_id + "/repository/commits?ref_name=" + this._branches[t],
			    headers: {
					'Private-Token': this._git_key
				},
				index: t
			};
			request.get(options, ((error, response, body) => {
				var index = response.request.index;
				var json = JSON.parse(body);
				var num = 0;
				if(json.length == 0){
					return;
				}
				while(num < json.length && this._last_commit[this._branches[index]] != json[num].id){
					num++;
				}
				if(num == 0){
					return;
				} else {
					var new_commits = [];
					var auth = json[0].author_name;
					var commits = "";
					for(var i = 0; i < num; i++){
						new_commits.push(json[i]);
						if(auth.indexOf(json[i].author_name) == -1){
							auth = auth.concat(", " + json[i].author_name);
						}
						commits = commits.concat("\t("+json[i].short_id+") " + json[i].title+ "\n");
					}
					var msg = "```New Commit" + ((num!=1) ? "s" : "") + " by " + auth + "\n" +
							"Commits: \n" + commits + "```";
					var guild = client.guilds.find('id', this._id);
					var channel = guild.channels.find("name", this._branches[index]);
					if(channel != null){
						channel.send(msg);
						this._last_commit[this._branches[index]] = json[0].id;
					}
				}
			}));
		}
	}
	/*
	 *	Check if the server is still valid
	 */
	checkAlive(){
		if(!client.guilds.exists("id", this._id)){
			delete servers[this._id];
			return false;
		}
		return true;
	}
	/*
	 *	Check if url is valid
	 */
	checkUrl(){
			var options = {
				url: this._url + "api/v4/version",
				headers: {
					'Private-Token': this._git_key
				}
			}
			request.get(options, (error, response, body) => {
				if(body == null){return false}
				var json = JSON.parse(body);
				return (json.version != null);
			});
	}


	/*
	 *	Let the server object start doing work
	 */
	start() {
		if(this._pid == ""){
			return "Invalid project id";
		} 
		if(this._git_key == ""){
			return "Empty token";
		}
		if(this.checkUrl()){
			return "Failed to authenticate with server";
		}
		setInterval(() => {
			if(this.checkAlive()){
				this.getPullReq(); 
				this.updateBranches();
				this.getCommits();
				client.guilds.find('id', this._id).sync();
			}
		}, 5000);
		return "";
	}
}

var servers = {};

/*
 *	Load servers after the client login
 */
function loadFromFile(){
	var load = JSON.parse(fs.readFileSync('servers.json', 'utf8'));
	var _server = {};
	Object.keys(load).forEach(function(key) {
		var _tmp = new Server(load[key]._id);
		_tmp.loadData(load[key]);
		_tmp.start();
		_server[key] = _tmp;
	});
	//console.log("Loaded: " + JSON.stringify(_server));
	return _server;
}

/*
 *	Save current servers to a json file for recovery
 */
function saveStateToFile(){
	fs.writeFile("servers.json", JSON.stringify(servers), function(err) {	    
		if(err) {
			return console.log(err);		
		}
	}); 
}
setInterval(() => {saveStateToFile();}, 30000);


/*
 * 	Check if we already have a 
 * 	Server obj for a given guild id
 */
function checkServer(id){
	if(!(id in servers)){
		var newServer = new Server(id);
		servers[id] = newServer;
	}
}

/*
 *	Message event handler
 */
client.on('message', message => {
	var id = message.member.guild.id;
	checkServer(id);
	if (message.content === '!git') {
		var msg = "```Git-Bot Commands:" + 
				"\n\t!git url <url of gitlab> (default: https://gitlab.com/)" + 
				"\n\t!git token <gitlab api token> (personal access token)" +
				"\n\t!git pid <project id> (found in project settings -> general)" + 
				"\n\t!git init (let Git-Bot know to start working)" +
				"```";
		message.channel.send(msg);
	}
	if (message.content === "cookie") {
		message.channel.send(":cookie:");
		message.delete();
	}
	if (message.content === "cookies") {
		message.channel.send(":cookie::cookie:");
		message.delete();
	}
	if ( message.author == client.guilds.find('id', id).owner.user){
		if (message.content === '!git init') {
			var msg = servers[id].start();
			if(msg != ""){
				message.channel.send(msg);
			}
		}
		if (message.content.startsWith('!git url')) {
			servers[id]._url = message.content.substring(9);
			message.delete();
		}
		if (message.content.startsWith('!git token')) {
			servers[id]._git_key = message.content.substring(11);
			message.delete();
		}
		if (message.content.startsWith('!git pid')) {
			servers[id]._repo_id = message.content.substring(9);
			message.delete();
		}
		if (message.content === "!git dump") {
			console.log(servers);
		}
	}
});

/*
 *	Client Startup
 */
client.on('ready', () => {
	var t = Math.floor(Date.now() / 1000);
	console.log(t + ": I am Alive!");
	client.sync = true;
	servers = loadFromFile();
});
