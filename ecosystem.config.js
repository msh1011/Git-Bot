module.exports = {
  apps : [
    {
      name      : 'Git-Bot',
      script    : 'git-bot.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    },
  ],
  deploy : {
    production : {
      user : 'node',
      host : '172.31.37.80',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:msh1011/Git-Bot.git',
      path : '/home/node/production',
      "post-setup": "ls -la",
	  'post-deploy' : 'npm install && pm2 restart git-bot.js'    }
  }
};
